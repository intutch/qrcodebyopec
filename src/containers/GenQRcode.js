import React, { Component } from 'react';
import { PrintQRcode } from '../companents/index'
import ReactToPrint from "react-to-print";
import qrcodes from 'qrcode'
const mockData = ['10001','10002','10003','10004','10005','10006','10007','10008','10009','10010','10011','10012','10013','10014','10015','10016','10017','10018','10019','10020','10021','10022','10023','10024','10025','10026','10027','10028','10029','10030','10031','10032']
export default class GenQRcode extends Component {
    state = {
        URLQRcodes: [],
        componentRef: "",
        size: 5
    }
    
    handleSize = (e) => {
        this.setState({
            size: e.target.value
        })
    }

    genqrcodes = async (data) => {
        try {
            await data.map(async (value, index) => {
                let URLdata = await qrcodes.toDataURL(value, { errorCorrectionLevel: 'M', version: 2 })
                this.setState({
                    URLQRcodes: [...this.state.URLQRcodes, { index: value, URLdata }]
                })
            })
        } catch (err) {
            console.log(err)
        }
    }
    componentDidMount() {
        this.genqrcodes(mockData)
    }
    render() {
        return (
            <div>
                <p>sizeQRcode </p> <input value={this.state.size} onChange={(e) => this.handleSize(e)} />
                <ReactToPrint
                    trigger={() => <a ><button href="#" className="button-secondary pure-button">print</button></a>}
                    content={() => this.componentRef}
                />
                <PrintQRcode {...this.state} ref={el => (this.componentRef = el)} />
            </div>
        )
    }
}